FROM korekontrol/docker-jnlp-slave-docker

USER root

ENV BUILDAGAIN=1

RUN curl -sL "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-linux-x86_64" -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose

RUN apt-get update \
  && apt-get install -y apt-transport-https
RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg \
  | apt-key add -
RUN echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" \
  | tee -a /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update \
  && apt-get install -y kubectl gettext-base

RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz

RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh

ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

RUN chown -R jenkins:docker /home/jenkins/.config

USER jenkins
